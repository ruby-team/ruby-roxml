require 'rspec/core/rake_task'
require 'rake/testtask'

RSpec::Core::RakeTask.new(:spec) do |spec|
  spec.pattern = FileList.new('./spec/**/*_spec.rb') do |fl|
    fl.exclude(/regression/)
  end
  spec.ruby_opts = "-Iexamples"
end

Rake::TestTask.new(:test) do |test|
  test.test_files=Dir["./test/**/*_test.rb"]
end

task :default => [:spec, :test]
